<?php

namespace Bank;

class Cheque
{
  protected $lookupTable = array (
    1 => "Ein",
    2 => "Zwei",
    3 => "Drei",
    4 => "Vier",
    5 => "Fünf",
    6 => "Sechs",
    7 => "Sieben",
    8 => "Acht",
    9 => "Neun",
    10 => "Zehn",
    11 => "Elf",
    12 => "Zwölf",
    13 => "Dreizehn",
    14 => "Vierzehn",
    15 => "Fünfzehn",
    16 => "Sechzehn",
    17 => "Siebzehn",
    18 => "Achtzehn",
    19 => "Neunzehn",
    20 => "Zwanzig",
    30 => "Dreißig",
    40 => "Vierzig",
    50 => "Fünfzig",
    60 => "Sechzig",
    70 => "Siebzig",
    80 => "Achtzig",
    90 => "Neunzig"
  );

  protected $strategies= array (
    'lookupStaticStrategy',
    'decomposeStrategy',
    'decomposeBigStrategy'
  );

  protected function lookupStaticStrategy($number)
  {
    if (array_key_exists($number, $this->lookupTable))
      return $this->lookupTable[$number];
    return false;
  }

  protected function lookupPostfix($number)
  {
    if (array_key_exists($number, $this->postfixes))
      return $this->postfixes[$number];
    return false;
  }

  protected function decomposeStrategy($number)
  {
    if ($number >= 100)
      return false;

    $lowerPart = $number % 10;
    $higherPart = $number - $lowerPart;
    $lowerWord = $this->lookupStaticStrategy($lowerPart);
    $higherWord = $this->lookupStaticStrategy($higherPart);
    return $lowerWord . "-und-" . $higherWord;
  }

  protected function decomposeBigStrategy($number)
  {
    $lowerPart = $number % 100;
    $higherPart = ($number - $lowerPart) / 100;
    $lowerWord = $this->decomposeStrategy($lowerPart);
    $higherWord = $this->lookupStaticStrategy($higherPart);
    return $higherWord . "-Hundert";
  }

  public function transcribe($number)
  {
    $strategy = current($this->strategies);
    do {
      $strategyResult = $this->$strategy($number);
      if ($strategyResult !== false)
        return $strategyResult;
    } while (($strategy = next($this->strategies)) !== false);

    return false;
  }
}
