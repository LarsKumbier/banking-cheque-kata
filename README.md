# In Worten Kata #

A solution for the Banking Check Kata


# Requirements #

* php
* [composer](http://getcomposer.org)


# Installation #

* `composer install`


# Development #

* Tests are done via PHPUnit in the `/tests/`-directory
* Sourcecode is in `/lib/`
* Run test suite via `vendor/bin/phpunit`
