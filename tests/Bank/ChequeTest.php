<?php

namespace Bank;

class ChequeTest extends \PHPUnit_Framework_TestCase
{
  public function setUp()
  {
    $this->cheque = new Cheque();
  }

  /**
   * @dataProvider dataprovider_IntegerInWords
   */
  public function test_IntegerInWords($input, $expected)
  {
    $actual = $this->cheque->transcribe($input);
    $this->assertSame($expected, $actual);
  }


  public function dataprovider_IntegerInWords() {
    return array (
      // Strategy: Static Lookup
      array(1, "Ein"),
      array(20, "Zwanzig"),
      array(90, "Neunzig"),

      // Strategy: Composite & Lookup
      array(21, "Ein-und-Zwanzig"),
      array(22, "Zwei-und-Zwanzig"),
      array(69, "Neun-und-Sechzig"),
      array(96, "Sechs-und-Neunzig"),

      // Strategy: Composite 100
      array(100, "Ein-Hundert"),
    );
  }
}
